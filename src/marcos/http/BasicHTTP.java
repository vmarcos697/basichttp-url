package marcos.http;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BasicHTTP {

    public static void main(String[] args) {
        try{
            URL u = new URL("https://www.bbc.com/");
            HttpURLConnection c=(HttpURLConnection) u.openConnection();

                if (c.getResponseCode()==200){
                    InputStream i=c.getInputStream();
                    StringBuffer sb = new StringBuffer();
                    BufferedReader br = new BufferedReader(new InputStreamReader(i));
                    String line = br.readLine();
                    while (line !=null){
                        System.out.println(line);
                        line = br.readLine();
                    }
                }

        } catch (Exception e){
            System.out.println(e);
        }

    }

}
