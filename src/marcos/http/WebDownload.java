package marcos.http;

import java.io.*;
import java.net.*;


public class WebDownload {

    //Creating a method, creating the URL object
    public void makeDownload(){
        try {
            URL url = new URL("https://www.google.com/");
            BufferedReader bf = new BufferedReader(new InputStreamReader(url.openStream())); //OpenStream is a method of the object to open the connection with the URL
            File file = new File("Downloaded.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));  //Constructor FileWriter allows to write to a file
            String string;
                while ((string = bf.readLine())!=null){     //While is reading and the reading is different than null, then write in the file "Downloaded.txt"
                    bw.write(string);
                }
                bf.close();
                bw.close();
                System.out.println("File created successfully!");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        WebDownload wd = new WebDownload();
        wd.makeDownload();

    }

}
